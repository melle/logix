---
title: Home
---
## card10 handout: there are no card10 badges being handed out, still compiling! You can exchange white vouchers against card10s at the card10 assembly at Day1, 18:00.

# card10

Welcome to card10 Wiki. You can [browse](https://git.card10.badge.events.ccc.de/card10/logix) this wiki source and [read more](/en/gitlab) about our GitLab instance.

**card10** is the name of the badge for the 2019 [Chaos Communication Camp](https://events.ccc.de/camp/2019/wiki/Main_Page). 

### [Getting started](/en/gettingstarted) 
You just received your card10, what now? The [Getting started](/en/gettingstarted) page has some advice how to assemble and use the card10.

### [First interhacktions](/en/firstinterhacktions)
After playing around with card10 a bit, you want to learn how to write your own software. Even if you have never programmed before, it is super easy to make some LEDs blink on card10 :)

### [tl;dr](/en/tldr)
Spare me the details just give me the links to the docs!

## Overview

> <img class="center" alt="Photo of the card10 badge" src="/media/ECKyISOXsAYDJZl.jpg"  width="420" height="auto" align="center">
>
> Hello, my name is card10

### Community
  - [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat): [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge)
  - or [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)) (mirror): [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr)
  - [GitLab: `https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

### Social Media
  - [`@card10badge@chaos.social`](https://chaos.social/@card10badge)
  - [`twitter.com/card10badge`](https://twitter.com/card10badge)

### [FAQ](/en/faq)
<!--- Please add this also to en/events.md -->

### Events
  - Ongoing:
     - **card10 reworking/soldering/firmware/etc.**: card10 village (R8):
       - firmware work, please bring your laptop with a clone of the firmware git and the [toolchain](firmware.card10.badge.events.ccc.de/how-to-build.html)
  - Upcoming:
     - **card10 firmware workshop**: card10 village (R8), Sun 18.08. 16:00
     - **card10 [talk](https://fahrplan.events.ccc.de/camp/2019/Fahrplan/events/10365.html)**: Curie Wed 21.08. 12:00

