---
title: Developing
---

Here is the source:
- [git.card10.badge.events.ccc.de/card10/companion-app-android](https://git.card10.badge.events.ccc.de/card10/companion-app-android/)
- [git.card10.badge.events.ccc.de/card10/companion-app-ios](https://git.card10.badge.events.ccc.de/card10/companion-app-ios/)


### Android / F-Droid nightly
The GitLab delivere a (unsafe) Android Debug Build of Companion App by a F-Droid repository.
If you are able to create debug logs and create Issues feel free to subscribe this repository.


[![https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/icon.png)](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo)


