# Los geht's!

Du hast also gerade dein card10 bekommen. Und jetzt?

## card10 zusammen bauen

[Hier](/en/assembleyourcard10) ist eine schritt-für-schritt-anleitung mit Bildern, um dein card10 zusammen zu bauen.

## card10 navigation
Es gibt vier Knopfe auf dem card10 _harmonic_ board: Oben links, direkt über dem USB-C-Connector ist der POWER-Knopf. Darunter ist der LEFT Knopf. Die Knöpfe auf der rechten Seite sind näher beieinander. Auf dieser Seite ist der obere Knopf der SELECT-Knopf und der untere der RIGHT Knopf.
 
<img class="center" alt="Zeichnung des card10 mit Knopf-Namen" src="/media/card10buttons.svg"  width="220" height="auto" align="center">


### Das card10 an und ausschalten

Um das card10 anzuschalten, drücke kurz auf den POWER knopf. Um es wieder auszuschalten, drücke den Knopf und halte ihn für mehrere Sekunden. (Üblicherweise sollte das ungefähr drei Sekunden lang dauern, aber für manche Apps kann es mehr als zehn Sekunden sein)

### Apps starten

Wenn dein card10 angeschaltet ist, bringt dich ein kurzer Druck auf den POWER-Knopf ins App-Menü. Du kannst mit dem RIGHT-Knopf runter- und mit dem LEFT-button hochscrollen. Der SELECT-Knopf startet die ausgewählte app. Der POWER-Knopf startet die app namens 'main.py', die auch die App ist, die startet, wenn du dein card10 anschaltest.

### USB Massenspeicher

Dein card10 kann ähnlich wie ein USB-Stick agieren, wenn du es im _USB Massenspeicher-Modus_ startest. Du kannst diesen Modus verwenden, um _Konfigurationsdateien_ und _micropython-skripte_ hochzuladen. Um in den in _USB Massenspeicher-Modus_ zu kommen, schalte dein card10 zunächst aus, indem du lange auf den POWER-Knopf drückst. Dann halte den RIGHT-Knopf und den POWER-Knopf gleichzeitig, bis der Bildschirm den Text "USB activated. Ready." anzeigt.

Wenn du jetzt dein card10 per USB an den Laptop anschließt, wird es als Massenspeichergerät angezeigt. Vergiss nicht, das card10 Massenspeichergerät wieder auszuwerfen.
Der USB-Massenspeicher-Modus wird beendet, indem du kurz auf den POWER-Knopf drückst.

### Deinen Nickname setzen
Um deinen nickname zu setzen, erschaffe auf deinem Laptop eine Datei, die `nickname.txt` heißt.
Starte dein card10 im USB-Massenspeichermodus, dann kopiere `nickname.txt` auf das card10. 
Jetzt halte nach einer App namens 'nickname.py' (TODO: evtl muss diese App zuerst heruntergeladen werden...), 
Diese App zeigt dienen Nickname an..

### LEDs zum Blinken bringen.



