Hinweis: Die authoritative Seite ist die englische. Wenn du hier etwas inhaltlich änderst, bitte sorge dafür, dass es auch auf der englischen Seite geändert wird. 

# Erste Interhacktionen

Die einfachste Art und Weise, um LEDs zum blinken zu bringen, ist, python zu benutzen. Es gibt zwei Möglichkeiten, um Python-Code auszuführen: Entweder du verwendest eine interaktive Python-Kommandozeile oder du speicherst `.py`-Dateien auf dem card10. Die interaktive Kommandozeile ist sehr praktisch, um Python-Kommandos auszuführen und `.py`-Dateien zu debuggen. Wenn du.py-Dateien direkt speicherst, sind die Skripte, die du schreibst auch dann verfügbar, wenn card10 nicht mehr mit deinem Computer verbunden ist.

## Die interaktive Kommandozeile benutzen
Du wirst mit der Kommandozeile arbeiten. Alles was du brauchst, um etwas in einer Kommandozeile einzugeben, wird so geschrieben `echo "hello"`
<!-- hier wäre ein Link zu Kommandozeilen-Tutorials nett -->

Du brauchst:
- einen Laptop mit einem freien USB-A oder USB-C-Port 
- je nachdem was du für USB-Konnektoren auf deinem Laptop zur Verfügung hast, ein USB-A-zu-USB-C-Kabel oder ein USB-C-zu-USB-C-Kabel
<!-- usb-A zu usb-C und usb-C zu usb-C photos wären hier hilfreich -->
- Eine Anwendung, um eine serielle Verbindung zu öffnen, zum Beispiel screen (installation über Kommandozeile unter Linux: `apt install screen`) oder picocom  installation über Kommandozeile unter Linux: `apt install picocom`)

### Wie man auf die Interaktive Python-Kommandozeile gelangt
Wechsle zum card10

Wenn dein card10-display den Text _USB activated._ anzeigt, ist dein card10 im _USB storage_-Modus. Das ist der falsche Modus für die interaktive Python-Kommaondozeil. Wenn das Gerät als Datenträger angemeldet ist, musst du es zuerst auswerfen. Ein kurzer Druck auf den _power_-Knopf sorgt dafür dass der _USB storage_-Modus verlassen wird.
<!-- hier wäre ein Bild nett, das den rechten Button anzeigt -->

Um auf die interaktive Kommandozeile zu gelangen, verbinde das card10 mit deinem Laptop. Üblicherweise wird es unter Linux `/dev/ttyACM0` oder `/dev/ttyACM1`, auf dem Mac `/dev/tty.usbmodem14101` genannt sein. 
Du kannst herausfinden, wie es auf deinem Gerät heisst, indem du die Ausgabe von `ls /dev/tty*` (Mac: `ls /dev/tty.*`) vergleichst, bevor und nachdem du card10 mit deinem Laptop verbunden hast.

Je nachdem welches Werkzeug du benutzt, kannst du die interaktive python-Kommandozeile öffnen. Möglicherweise musst du "sudo" vor die Kommandos setzen
- mit screen: `screen /dev/ttyACM0 115200`
- mit picocom: `picocom -b 115200 /dev/ttyACM0`


Wenn du keine Zeile siehst, die mit _>>>_ beginnst, versuche _Ctrl+C_ zu drücken (vielleicht öfter als einmal).

<div class="p-notification--information">
  <p class="p-notification__response">
    <span class="p-notification__status">Hinweis:</span>Die erste Zeile gibt dir die Version deiner Firmware, z.B. "MicroPython v1.11-37-g62f004ba4 on 2019-08-17; card10 with max32666".
  </p>
</div>


Sobald du eine Zeile hast, die mit _>>>_ beginnt, bist du bereit, mit Python zu programmieren!

### Eine LED zum blinken bringen

Es gibt ein paar nette Python-Funktionen, die bereit stehen, um die LEDs zum blinken zu bringen. Importieren wir erstmal die Bibliothek mit diesen Funktionen, indem wir eingeben:

```python
import leds

# Als nächstes bringen wir eine der raketen zum leuchten, indem wir eingeben:
>>> leds.set_rocket(0, 31)
```

Die erste Zahl, die eingegeben wird, ist der Index, um die Raketen-LED auszuwählen. Du kannst auswählen zwischen 0 (blau, links), 1 (gelb, mittel) und 2 (grün, rechts)..


## Card10 im USB-Massenspeicher-Modus benutzen

### Closing a session
Um eine Sitzung in screen zu beenden, drücke `Strg+A` dann `k` dann `y`.  In picocom ist die Tastenkombination `Strg+A`, dann `Strg+Q`.

## Glückwunsch!
Wenn du deine neu erworbenen card10-Skills anwendest, schau dir bitte die [generellen überlegungen, um Interhacktions zu designen](/de/interhacktions) an. Denk ausserdem daran, dass manche blinke-Muster bereits eine besondere Bedeutung haben, um den [persönlichen Zustand](/ps) einer reisenden person anzuzeigen .
