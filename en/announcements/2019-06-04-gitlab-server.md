---
title: New GitLab Server
date: 2019-06-04
type: blog
---

We moved from our [Hamburg GitLab](https://gitlab.hamburg.ccc.de/card10/) to:

> [`https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

1. Please be aware this hosting is a work in progress. If you encounter any issues please let us know on Matrix or IRC (see below).
2. We are aware and working on GDPR compliance, for progress you can track this [issue](https://git.card10.badge.events.ccc.de/card10/meta/issues/1)
3. Social-Sign-On (Twitter, Github, ...) is coming soon
